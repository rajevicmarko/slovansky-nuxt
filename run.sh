#!/bin/sh

npm install
npm run build
pm2 delete slovansky
pm2 start npm --name "slovansky" -- start
sudo cp nginx/slovansky.devplayground.space /etc/nginx/sites-available/slovansky.devplayground.space
sudo ln -s -f /etc/nginx/sites-available/slovansky.devplayground.space /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx
pm2 save