const webpack = require('webpack')

module.exports = {
    /*
     ** Headers of the page
     */
    head: {
        title: 'Slovanský dům',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Slovanský dům' }
        ],
        link: [{
            rel: 'icon',
            type: 'image/png',
            href: '/favicon.png'
        }],
        script: [
            { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js' }
            // { src: '//c.imedia.cz/js/retargeting.js' }
        ],
    },
    router: {
        middleware: 'i18n'
    },
    serverMiddleware: [
        '~/middleware/301redirect.js'
    ],
    css: [
        // Load a Node.js module directly (here it's a Sass file)
        // 'bootstrap',

        // SCSS file in the project
        '@/assets/css/main.scss',
        'swiper/dist/css/swiper.css'
    ],

    // CACHING
    cache: {
      max: 10000,
      maxAge: 9000000
    },

    // MODULES
    modules: [
        'nuxt-device-detect',
        // ['@nuxtjs/google-tag-manager', { id: 'GTM-120239067' }],,
        ['@nuxtjs/google-analytics', {
            id: 'UA-120239067-1'
        }],
        ['@nuxtjs/pwa',{
            meta: {
                ogType: false,
                ogTitle: false,
                ogDescription: false
            }
        }
       ]

        // '@nuxtjs/axios',
        // '@nuxtjs/pwa',
    ],

    // Axios config
    axios: {
        // proxyHeaders: false
    },

    /*
     ** Customize the progress bar color
     */

    loading: {
        color: '#4e84f1',
        failedColor: '#4e84f1',
        height: '2px'
    },

    /*
     ** Build configuration
     */
    router: {
        extendRoutes(routes, resolve) {

            // @ TO BE DONE
            // here we may fetch the pages and create the custom routes

            routes.push({
                name: 'custom',
                path: '*',
                component: resolve(__dirname, 'pages/_page.vue')
            })
        }
    },

    build: {

        vendor: ['vee-validate', 'vue-i18n', 'babel-polyfill'],

        babel: {
            presets: [
                ['vue-app', {
                    useBuiltIns: true,
                    targets: { ie: 9, uglify: true }
                    }
                ]
            ]
        },
        extractCSS: {
            allChunks: true
        },

        /*
         ** Run ESLint on save
         */
        extend(config, { isDev, isClient }) {

            const urlLoader = config.module.rules.find((rule) => rule.loader === 'url-loader')
            urlLoader.test = /\.(png|jpe?g|gif)$/

            config.module.rules.push({
                test: /\.svg$/,
                loader: 'vue-svg-loader',
                exclude: /node_modules/
            })


            if (!isClient) {
                // This instructs Webpack to include `vue2-google-maps`'s Vue files
                // for server-side rendering
                config.externals.splice(0, 0, function(context, request, callback) {
                    if (/^vue2-google-maps($|\/)/.test(request)) {
                        callback(null, false)
                    } else {
                        callback()
                    }
                })
            }

            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    // exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/
                    exclude: /(node_modules)/
                })
            }

        },
        plugins: [
            new webpack.ProvidePlugin({
                '_': 'lodash',

            })
        ],
    },

    env: {
        apiUrl: 'https://sdapi.devplayground.space/',
        defaultTitle: 'Slovanský dům',
        defaultBlogTitle: 'Svět Slovanského domu',
        defaultDescription: 'Kvetoucí zahrada módy a zábavy',
        ogUrl: 'https://slovanskydum.cz/',
        defaultImage: 'https://slovanskydum.cz/ogDefault.jpg',
        defaultLocale: "cs_CZ"
    },


    plugins: [
        '~plugins/vee-validate.js',
        '~/plugins/common',
        '~/plugins/vuex-router-sync',
        '~/plugins/framework',
        '~/plugins/i18n.js',
        '~/plugins/siteConfig.js',
        '~/plugins/eventBus.js',
        '~/plugins/social-share.js',
        '~/plugins/gmaps.js',
        // '~/plugins/axios',
        // '~/plugins/cachedApi',
        { src: '~/plugins/globalComponents.js', ssr: false },
        { src: '~/plugins/nonssr.js', ssr: false },
        { src: '~/plugins/routerHistory.js', ssr: false },
        { src: '~plugins/ga.js', ssr: false },
        { src: '~/plugins/split.js', ssr: false },
        { src: '~/plugins/select.js', ssr: false },
        { src: '~/plugins/instagram.js', ssr: false },
        { src: '~/plugins/nuxt-swiper-plugin.js', ssr: false },
        { src: '~/plugins/vue-masonry', ssr: false },
        { src: '~/plugins/vue-imagesLoaded', ssr: false }
    ]

}
