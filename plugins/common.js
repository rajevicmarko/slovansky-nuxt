// Common functions
import Vue from 'vue';
var removeDiacritics = require('diacritics').remove;

const common = {
   install(options) {


      String.prototype.replaceAll = function(search, replacement) {
          var target = this;
          return target.replace(new RegExp(search, 'g'), replacement);
      };

      String.prototype.sanitize = function() {
         let g = removeDiacritics(this);

         return g.toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '')
      }

      
      Vue.prototype.convertToSlug = (text) => {
         return text ? text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '') : null
      }

      // capitalize first letter
      Vue.prototype.capitalize = (string) => {
         if (typeof string !== "string") {
            return string;
         }
         return string.charAt(0).toUpperCase() + string.slice(1)
      }


      Vue.prototype.getHostName = (url) => {
          var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
          if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
          return match[2];
          }
          else {
              return null;
          }
      }


      // USE LIKE THIS --> this.loadImage(imgUrl).then(() => {});
      Vue.prototype.loadImage = (url) => {

        // console.log("PRELOAD", url)

         return new Promise((resolve, reject) => {
            var img = new Image()
            img.onload = () => {
               resolve(url)
            }
            img.onerror = () => {
               reject(url)
            }
            img.src = url
         })
      }

      // USE LIKE THIS --> this.loadImages([imgUrls]).then(() => {});
      Vue.prototype.loadImages = (options) => {

        let imageCount = 0
        let images = []

        if(!options.urls){
          options.callback && options.callback()
          return
        }

        options.urls.forEach(src => {  // for each image url
           const image = new Image();
           image.src = src;

           image.onload = ()=>{ 
               imageCount += 1;

               if(imageCount === options.urls.length){ // have all loaded????

                   options.callback && options.callback()

                   return images
               }
           }
           images.push(image); // add loading image to images array

        });

         // let promises = [];
         // _.each(urls, (url) => {
         //    let promise = new Promise((resolve, reject) => {
         //       var img = new Image()
         //       img.onload = () => {
         //          resolve(url)
         //       }
         //       img.onerror = () => {
         //          reject(url)
         //       }
         //       img.src = url
         //    })
         //    promises.push(promise);
         // });

         // return promises;
      }


   }
};

Vue.use(common);
