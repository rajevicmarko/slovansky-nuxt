// export default function ({ $axios, redirect }) {
//   $axios.onRequest(config => {
//     console.log('Making request to ' + config.url)
//   })

//   $axios.onError(error => {
//     const code = parseInt(error.response && error.response.status)
//     if (code === 400) {
//       redirect('/400')
//     }
//   })
// }



import { cacheAdapterEnhancer } from "axios-extensions"
import LRUCache from "lru-cache"
const ONE_HOUR = 1000 * 60 * 60

const defaultCache = new LRUCache({ maxAge: ONE_HOUR })

export default function({ $axios }) {

  const defaults = $axios.defaults
  // https://github.com/kuitos/axios-extensions
  defaults.adapter = cacheAdapterEnhancer(
    defaults.adapter,
    true,
    "useCache",
    defaultCache
  )
}