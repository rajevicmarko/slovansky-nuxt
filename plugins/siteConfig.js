// site config

import Vue from 'vue'
Vue.mixin({
	data () {
	    return {
	    	siteName: "Papirna",
        	apiUrl: "http://apipapirna.devplayground.space/",
			defaultLocale: "en_US",
			instagramUrl: "https://www.instagram.com/",
			facebookUrl: "https://www.facebook.com/",
	    }
  	}
})