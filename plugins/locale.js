var locales = {
  isLocale(segment) {
     let r = false;

     let localeRegExp = new RegExp(/^[a-z]{2}_[A-Z]{2}$/)

     if (localeRegExp.test(segment)) {
        r = true;
     }

     return r;
  },

  getLocale(route) {
     route = route.substr(1, route.length) // remove the first slash
     let routeSegments = route.split('/')

     let firstSegment = routeSegments[0]
     let locale;

     if (locales.isLocale(firstSegment)) {
        locale = firstSegment;
        routeSegments.shift();
     } else {
        locale = process.env.defaultLocale
     }

     return locale;
  },

  getPageModel(store){
       let route = store.state.route.path
       let routeSegments = route.split('/')
       let firstSegment = routeSegments[1]
       let onlyLocale = routeSegments.length === 2 && locales.isLocale(firstSegment)
       console.log('route route', route);
       let page = {};
       if (!route.length || route.length < 2 || onlyLocale) {
           page = store.getters['pages/getByProp']('attribute_set_code', 'home')
       } else {
           page = store.getters['pages/getByUrl'](route)
       }

       return page
  },

  is404(store){
    return store.getters['app/getState']('is404')
  }
}

module.exports = {
  isLocale: locales.isLocale,
  getLocale: locales.getLocale,
  getPageModel: locales.getPageModel,
  is404: locales.is404
};
