import Vue from 'vue'
import Parallax from '@/components/behaviours/Parallax';
import Waypoint from '@/components/behaviours/Waypoint';
// import MouseParallax from '@/components/behaviours/Mouseparallax';
// import Card3d from '@/components/behaviours/Card3d';
// import AnimatedLetters from '@/components/behaviours/AnimatedLetters';
import CustomTransition from '@/components/CustomTransition';
// import PageFooter from '@/components/shared/PageFooter';
// import ContactComponent from '@/components/shared/ContactComponent'
// import FormGenerator from '@/components/form/FormGenerator'

const components = {
	Parallax,
	CustomTransition,
	Waypoint
	// AnimatedLetters,
	// PageFooter
	// ContactComponent,
	// FormGenerator,
	// MouseParallax,
	// Card3d
}

Object.keys(components).forEach(key => {
  Vue.component(key, components[key])
})