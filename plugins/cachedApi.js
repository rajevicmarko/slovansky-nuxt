import axios from './axios'
let data // simple data cache


let includes = [
 "fields.image",
 "fields.gallery",
 "fields.featured_image",
 "fields.floorplan",
 "fields.logo",
 "fields.pdf_brochure",
 // "fields.location_marker", 
 // "fields.location_category"
].join(",");

const cachedApi = {
  getData (locale) {
  	let url = process.env.apiUrl + 'api/delivery/entities?locale=' + locale + '&include=' + includes

    return new Promise(function (resolve, reject) {
      if (data) { // get data from cache if exists
        resolve(data)
      } else { // else get data from API
        axios
          .request({
            url: url
          })
          .then(response => {
            data = response.data // set data, so new getData() will take data from cache
            resolve(data)
          })
          .catch(reject)
      }
    })
  }
}




// let data

// function getData ({$axios}, locale) {

//    let url = process.env.apiUrl + 'api/delivery/entities?locale=' + locale + '&include=' + includes

//    let includes = [
//      "fields.image",
//      "fields.gallery",
//      "fields.featured_image",
//      "fields.floorplan",
//      "fields.logo",
//      "fields.pdf_brochure",
//      // "fields.location_marker", 
//      // "fields.location_category"
//   ].join(",");

//   return new Promise(function (resolve, reject) {
//     if (data) {
//       resolve(data)
//     } else {
//       $axios.get(url)
//         .then(response => {resolve(response.data)
//         .catch(reject)})
//     }
//   });

// }
// module.exports = getData