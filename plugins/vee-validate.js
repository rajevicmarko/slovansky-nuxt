import Vue from 'vue';
import VeeValidate, {Validator} from 'vee-validate';
import en from '@/locales/forms/en_US';
import cs from '@/locales/forms/cs_CZ';


Validator.localize('cs_CZ', cs);
Validator.localize('en_US', en);


Vue.use(VeeValidate, {locale: cs});