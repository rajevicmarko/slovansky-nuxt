// import EventBus from '@/config/event-bus'
import Vue from 'vue'
import {isLocale, getLocale, getPageModel} from '~/plugins/locale'

var baseApp = {

  watch: {
    locale(val) {
      console.log("LOCALE CHANGED!!!") // NOT FIRING ON LOAD! WHY?????
      this.setLocale(val, true)
    },
    page(val) {

      if (!val) {
        return
      }

      this.$store.dispatch('app/SET_STATE', {
        activePage: val
      })
    },
    pages() {
      // pages changed == locale changed
      // find the old page and redirect to the same page on diff language
      let oldPage = this.$store.getters['app/getState']('activePage');

      this.$store.dispatch('pages/PARSE_PAGES')
      if (oldPage) {
        let newPage = this.$store.getters['pages/getByProp']('id', oldPage.id)
        if (newPage) {
          this.$router.push({ path: newPage.url })
        }
      }
    }
  },

  created() {
    let self = this

    let route = self.$store.state.route.path
    let locale = getLocale(route)


    self.$store.dispatch('app/SET_STATE', {
      locale: locale
    })

    // set locale
    self.setLocale(locale, false)


    self.$nextTick(() => {
      this.$store.dispatch('app/SET_STATE', {
        activePage: this.page
      })
    })
  },

  mounted() {
    var self = this

    self.resizeHandler()
    this.$validator.localize(this.locale);
    window.addEventListener('resize', self.resizeHandler)

    // set isIE in store
    self.$nextTick(() => {
      self.$store.dispatch('app/SET_STATE', {
        isIE: self.detectIE
      })
    })
  },

  computed: {
    locale() {
      return this.$store.getters['app/getState']('locale')
    },

    pages() {
      return this.$store.getters['pages/getPages']
    },

    parsedPages() {
      return this.$store.getters['pages/getParsedPages']
    },

    page() {
      return getPageModel(this.$store)
    },

    pageTemplate() {

      if (!this.page) {
        return 'NotFound'

     }

     let template = this.getAttribute(this.page, 'template') || this.findTemplateByAttrSet(this.page.attribute_set_code)

     // handle 404
     let templateExists = _.has(this.$options.components, this.capitalize(template))
     if (!templateExists) {
        template = 'NotFound'
     }
     return template
    },

    detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
           // Edge (IE 12+) => return version number
           return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }
  },

  methods: {

    resizeHandler() {
      let self = this;

      self.$nextTick(() => {
        self.$bus.$emit('resize')
      })

      self.$store.dispatch('app/SET_STATE', {
        width: window.innerWidth,
        height: window.innerHeight
      })

      let wrapper = self.$refs.wrapper

      if (!wrapper || !wrapper.length) {
        return false
      }

      self.$store.dispatch('app/SET_STATE', {
        wrapperHeight: wrapper.clientHeight
      })

      self.$store.dispatch('app/SET_STATE', {
        wrapperWidth: wrapper.clientWidth
      })
    },

    setLocale(locale, fetch) {
      fetch && this.$store.dispatch('pages/LOAD_PAGES', locale)
      this.$validator.localize(locale);
      this.$i18n.locale = locale;
    }
  }
}


export default baseApp;
