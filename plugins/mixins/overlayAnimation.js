
var overlayAnimation = {
  methods: {
     enterOverlay(el, done){
        let self = this

        self.enterOverlayTl = new TimelineMax({
          onComplete: done
        })

        self.enterOverlayTl.from(el, 1, {
           opacity: 0,
           // ease: 'Power4.easeOut'
        });
      },

      leaveOverlay(el, done){
        let self = this

        self.leaveOverlayTl = new TimelineMax({
          onComplete: done
        })

        self.leaveOverlayTl.to(el, 1, {
           opacity: 0,
        });
      }
  }
}

export default overlayAnimation;