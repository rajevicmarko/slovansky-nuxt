// import $ from 'jquery'
import imagesLoaded from 'imagesloaded';

import Scrollbar from 'smooth-scrollbar';
import PageFooter from '@/components/shared/Footer'

import OverscrollPlugin from '@/plugins/overscroll'
import Slider from '@/components/shared/Slider'
import GalleryText from '@/components/shared/GalleryText'
import PagePreview from '@/components/shared/PagePreview'
import BlogPreview from '@/components/home/BlogPreview'
import LocationPreview from '@/components/shared/LocationPreview'
import ContentBox from '@/components/shared/ContentBox'
import Offices from '@/components/shared/Offices'
import OfficesNavigator from '@/components/shared/OfficesNavigator'
import InstagramFeed from '@/components/shared/InstagramFeed'
// import OverscrollPlugin from 'smooth-scrollbar/plugins/overscroll'

var BasePage = {
  props: ['model'],

  data: () => ({}),


  // head() {
  //   let title = this.getAttribute(this.model, 'title')
  //   let image = this.getImageVersion(this.model, 'image')


  //   return {
  //     title: this.model.fields.meta_title || title,
  //     meta: [{
  //         hid: 'description',
  //         name: 'description',
  //         content: this.model.fields.meta_description || title
  //       },
  //       {
  //         hid: 'og:title',
  //         property: 'og:title',
  //         content: this.model.fields.meta_title || title
  //       },
  //       {
  //         hid: 'og:url',
  //         property: 'og:url',
  //         content: 'http://papirnabubenec.cz'
  //       },
  //       {
  //         hid: 'og:image',
  //         property: 'og:image',
  //         content: image || 'http://papirnabubenec.cz/ogDefault.jpg'
  //       },
  //       {
  //         hid: 'og:description',
  //         property: 'og:description',
  //         content: this.model.fields.meta_description || title
  //       }
  //     ]
  //   }
  // },


  mixins: [],

  components: {
    Slider,
    GalleryText,
    PagePreview,
    BlogPreview,
    LocationPreview,
    ContentBox,
    Offices,
    OfficesNavigator,
    InstagramFeed,
    PageFooter
  },

  computed: {
    self() {
      return this
    },
    pageComponents() {
      return this.getAttribute(this.model, 'components')
    },
    transitioning() {
        return this.$store.getters['app/getState']('transitioning')
    }
  },


  // async fetch ({ store, params }) {
  //   console.log("LOAD PAGES!")
  //   await store.dispatch('LOAD_PAGES');
  // },

  created: function() {},

  mounted: function() {
    var self = this;


    this.$nextTick(() => {
      this.resizeHandler();
    })

    if(!this.preventEvents){
      this.$store.dispatch('app/SET_STATE', { scrollTop: 0 });
    }

    imagesLoaded(this.$el, () => {
      self.resizeHandler()
    });

    this.$bus.$on('resize', self.resizeHandler)
  },

  destroyed() {},

  beforeDestroy() {

    if(this.scrollbar){
      this.scrollbar.removeListener();
      this.scrollbar.destroy();

    }

    this.$bus.$off('updateScrollbar');
    this.$bus.$off('scrollTo');

    if(!this.preventEvents){
      this.$store.dispatch('app/SET_STATE', { scrollTop: 0 });
    }
  },

  methods: {

    chooseComponentTemplate(component) {
      let tmpl;

      switch (component.attribute_set_code) {
         case 'gallery_text':
            tmpl = 'GalleryText'
            break

         case 'gallery':
            tmpl = 'GalleryText'
            break

         case 'content':
            tmpl = 'ContentBox'
            break;

         case 'page_preview':
            tmpl = 'PagePreview'
            break;

         case 'blog_preview':
            tmpl = 'BlogPreview'
            break;

         case 'offices':
            tmpl = 'Offices'
            break;

         case 'location_preview':
            tmpl = 'LocationPreview'
            break;

         case 'instagram_feed':
            tmpl = 'InstagramFeed'
            break;

         case 'offices_navigator':
            tmpl = 'OfficesNavigator'
            break;

         default:
            tmpl = this.capitalize(component.attribute_set_code)
            break
      }

      return tmpl
   },

      columnClass(index) {
        let colWidth = 12 / this.numberOfColumns;
        return 'column col-md-' + colWidth + ' column-' + index
      },
      calculatePlaxFactor(colIndex) {
      let factor

      return (colIndex + 1) % 2 == 0 ? -.1 : -.5
      },
      calculateRow(colIndex) {

      let numberOfVariations = 4;

      let boxes = _.filter(this.boxes, (cat, index) => {
          let row = Math.floor(index / numberOfVariations)
          let realIndex = index - (row * numberOfVariations);
          return realIndex == colIndex
      });

      return boxes;
      },

      resizeHandler() {
        if (!this.$refs.wrapper) {
          console.log('nema refs wrapper');
          return
        }

        this.$store.dispatch('app/SET_STATE', {
          pageHeight: this.$refs.wrapper.clientHeight,
          pageWidth: this.$refs.wrapper.clientWidth
        });
      },

    onScroll(e) {
      var self = this;
      self.$store.dispatch('app/SET_STATE', { scrollTop: e.target.scrollTop });
    },

    initSmooth(el) {
      var self = this;

      Scrollbar.use(OverscrollPlugin)
      var scrollbar = Scrollbar.init(el || this.$el, {
        damping: .1,
        renderByPixels: true,
        plugins: {
          overscroll: {
            enable: true,
            maxOverscroll: 150,
            damping: .2
          },
        },
      });
      // let ticking;
      if (this.isMobile) {
         this.$el.removeEventListener('scroll', self.onScroll);
         this.$el.addEventListener('scroll', self.onScroll)
         return
      }

      let $el = el || this.$el;
      console.log("el el", $el);
      var scrollbar = Scrollbar.init($el, {
         damping: .11,
         renderByPixels: true
      });

      this.$bus.$on('updateScrollbar', (e) => {
         scrollbar.update();
      });

      this.$bus.$on('scrollTo', (top) => {
         scrollbar.scrollTo(0, top, 600);
      });

      scrollbar.addListener((status) => {
         this.$store.dispatch('app/SET_STATE', { scrollTop: status.offset.y });
      });
   }



  }
}


export default BasePage;
