var imageAnimation = {
   methods: {

      enterImage(el, done, noOpacityChange, delay, preloadImages, scale, opacity) {
         let self = this

         // let img = el.style.backgroundImage.replaceAll("url", "").replaceAll("'", "").replaceAll('"', "")
         // .replaceAll("(", "").replaceAll(")", "")

         preloadImages = preloadImages ? preloadImages :
            el && el.length == 1 && el.getAttribute("data-url") ? [el.getAttribute("data-url")] :
            null;


         self.enterTl = new TimelineMax({
            onComplete: done
         })

         // let op = noOpacityChange ? 1 : 0
         let op = noOpacityChange ? 1 : 0

         self.enterTl.set(el, {
            scale: scale || 1.1,
            opacity: op
         })

         // let dur = scale ? 3.5 : 2

         self.loadImages({
            urls: preloadImages,
            callback: () => {
               // self.onImageLoaded(el, done, noOpacityChange, delay, scale)

               self.$bus.$emit('preloadedImage')

               self.enterTl.from(el, 2, {
                  scale: 1,
                  delay: delay,
                  ease: 'Power4.easeOut'
               });

               self.enterTl.to(el, 1, {
                  opacity: opacity || 1,
                  delay: delay,
               }, 0);

            }
         });

      },
      leaveImage(el, done) {
         let self = this

         this.$bus.$on('preloadedImage', () => {

            el.classList.add('transitioning-out');

            self.leaveTl = new TimelineMax({
               onComplete: done
            })

            self.leaveTl.to(el, 1, {
               opacity: 0,
               // scale: 1
            });

         })

      }
   }
}

export default imageAnimation;
