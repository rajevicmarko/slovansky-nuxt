var slider = {
   data: () => ({
      currentIndex: 0,
      sliderTransitioning: false,

      clipPathFull: 'polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)',
      clipPath: 'polygon(0 0, 100% 0%, 90% 100%, 0% 100%)'
   }),

   methods: {

      changeSlide(e) {

         if (this.sliderTransitioning) {
            return
         }

         let refY = 8

         if (e.deltaY < -refY) {
            console.log("onmousewheel prev:", e.deltaY)
            this.prevSlide()
         } else if (e.deltaY > refY) {
            console.log("onmousewheel next:", e.deltaY)
            this.nextSlide()
         }
      },

      nextSlide() {
         if (this.sliderTransitioning) {
            return
         }

         this.sliderTransitioning = true

         this.currentIndex = this.currentIndex >= 0 && this.currentIndex < this.slides.length - 1 ?
            this.currentIndex + 1 :
            0
      },

      prevSlide() {
         if (this.sliderTransitioning) {
            return
         }

         this.sliderTransitioning = true

         this.currentIndex = this.currentIndex > 0 && this.currentIndex < this.slides.length ?
            this.currentIndex - 1 :
            this.slides.length - 1
      },

      updateIndex(index) {
         this.currentIndex = index
      }
   },

   computed: {
      currentSlide() {
         return this.slides[this.currentIndex]
      }
   }
}

export default slider;
