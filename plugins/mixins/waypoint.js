
export default {
   name: 'waypoint',
   data: () => ({
      bounds: {},
      // topBound: 0,
      // height: 0,

      refBound: 0,
      refSize: 0,
      horizontal: false
   }),

   watch: {
      scrollTo(active) {},
      isVisible(visible) {
         if(this.model){
         }
      },
      pageRefSize(size) {
         // console.log("RESIZE!");
        //  this.waypointResize();
      },

      pageHeight(h) {
        this.waypointResize()
      },

      pageWidth(w){
        this.waypointResize()
      }
   },

   methods: {

      waypointResize(notWindowResize) {

         if (!this.$el || (this.$store.getters['app/getState']('transitioning') && self.tl) || this.isMobile || this.isiPad) {
            return
         }

         if (notWindowResize === true) {
            return;
         }

         this.bounds = this.$el.getBoundingClientRect()

         let prop = this.horizontal ? 'left' : 'top'

         this.refSize = this.horizontal ? this.$el.offsetWidth : this.$el.offsetHeight
         this.refBound = this.bounds[prop] + this.scrolled

         // console.log("DD", this.refBound, this.scrolled);

         // this.initWaypointTween && this.initWaypointTween();
      }
   },

   mounted() {
      var self = this;

      // EventBus.$on('resize', () => {
      //    self.resizeHandler()
      // })

      requestAnimationFrame(() => {
         self.waypointResize()
      })
   },

   computed: {
      scrolled() {
         let scroll = this.horizontal ? 'scrollLeft' : 'scrollTop'
         // console.log("SSS", this.$store.getters['app/getState'](scroll), scroll);

         return this.$store.getters['app/getState'](scroll)
      },

     pageHeight()  {
         return this.$store.getters['app/getState']('pageHeight')
     },

     pageWidth()  {
         return this.$store.getters['app/getState']('pageWidth')
     },

      offset() {
         return this.pageRefSize * 0.12
      },

      pageRefSize() {
         let s = this.horizontal ? 'width' : 'height'
         return this.$store.getters['app/getState'](s)
      },

      oneDirectionVisible() {
         const self = this

         if(this.isMobile || this.isiPad){
            return true;
         }


         let curOffset = this.refBound - this.scrolled


         let isVisible = (curOffset > this.pageRefSize - this.offset) ? false : true


         // console.log("D", this.refBound, this.scrolled, curOffset, this.pageRefSize, this.offset)

         return isVisible
      },

      isVisible() {
         const self = this

         if(this.isMobile || this.isiPad){
            return true;
         }

         let start = this.refBound - this.offset;
         let end = this.refBound + this.refSize - this.offset

         return this.scrolled >= start && this.scrolled < end
      }
   }
}
