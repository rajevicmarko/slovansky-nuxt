import $ from 'jquery-slim'

var textLinesAnimation = {
  methods: {
      // hl transitions
      enterHl(el, done, elems){
        this.splitLines(elems)
        this.initHlTween({
          inTransition: true,
          callback: done,
          elems: elems
        })
      },

      leaveHl(el, done){
        this.splitLines()
        this.initHlTween({
          inTransition: false,
          callback: done
        })
      },

      splitLines(elems) {
         let self = this

         if (!self.$refs.headline && !elems) {
            return
         }

         // split the headline lines
         self.mySplitText = new SplitText(self.$refs.headline || elems, {
            type: 'lines',
            linesClass: "lines"
         })


         // an array of all the divs that wrap each character
         self.lines = self.mySplitText.lines

         // wrap the lines with the overflow div
         $(self.lines).wrap('<div class="position-relative overflow-hidden"></div>')
      },

      initHlTween(options)  {
        let self = this

        self.hlTween = new TimelineMax({
            // paused: true,
            delay: options.delay,
            onComplete: () => {
              options.callback && options.callback()
            }
        })

        let fromY = options.inTransition ? -100 : 0
        let toY = options.inTransition ? 0 : 100
        let fromOp = options.inTransition ? 0 : 1
        let toOp = options.inTransition ? 1 : 0

        self.hlTween.staggerFromTo(self.lines, .6, {
            opacity: fromOp,
            yPercent: fromY,
            ease: 'Power2.easeOut'
        }, {
            opacity: toOp,
            yPercent: toY,
            ease: 'Power2.easeOut'
        }, -0.08)

      }
    }
}

export default textLinesAnimation;