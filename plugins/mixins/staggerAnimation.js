
var staggerAnimation = {
  methods: {
      enterStagger(el, done){
        let els = el.querySelectorAll('.animated')

        this.initStaggerTween(els, {
          inTransition: true,
          callback: done, 
          delay: 0.4
        })
      },

      leaveStagger(el, done){
        let els = el.querySelectorAll('.animated')

        this.initStaggerTween(els, {
          inTransition: false,
          callback: done
        })
      },
      // end :: content transitions


      initStaggerTween(els, options)  {
        let self = this

        self.staggerTween = new TimelineMax({
            // paused: true,
            delay: options.delay,
            onComplete: () => {
              options.callback && options.callback()
            }
        })
       
        let staggerDelay = options.staggerDelay ? options.staggerDelay 
                        : options.inTransition ? 0.15 
                        : 0
                        
        let fromOp = options.inTransition ? 0 : 1
        let toOp = options.inTransition ? 1 : 0
        let dur = options.inTransition ? 1 : 0.4

        self.staggerTween.staggerFromTo(els, dur, {
            opacity: fromOp,
        }, {
            opacity: toOp,
        }, staggerDelay)

      }
  }
}

export default staggerAnimation;