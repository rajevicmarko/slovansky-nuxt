import Vue from 'vue'
import Vuex from 'vuex'
import { isLocale, getLocale } from '~/plugins/locale'
import pages from './modules/pages';
import app from './modules/app';

Vue.use(Vuex)


export default () => {
  return new Vuex.Store({
    actions: {
      async nuxtServerInit ({ dispatch, commit, state }, { req }) {

        let route = state.route.path
        let locale = getLocale(route)

        console.log("Current route:", route)
        console.log("Current locale:", locale)
        console.log("FETCHING PAGES...")

        await dispatch('pages/LOAD_PAGES', locale)
      }
    },
    modules: {
      pages,
      app
    }
  })
}