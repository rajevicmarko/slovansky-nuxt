// import axios from 'axios';
import cachios from 'cachios';
// import { cacheAdapterEnhancer, throttleAdapterEnhancer } from 'axios-extensions';

const mutations = {
   SET_STATE: (state, { prop, value }) => {
      state[prop] = value
   }
};

const actions = {

   SET_STATE({ commit }, obj) {
      var value = Object.values(obj)[0];
      var prop = Object.keys(obj);
      commit('SET_STATE', { prop: prop, value: value })
   },

   LOAD_PAGES({ commit, dispatch }, locale) {

      let includes = [
        "fields.image",
        "fields.gallery",
        "fields.logo"
    ].join(",");

      return cachios.get(process.env.apiUrl + 'api/delivery/entities?locale=' + locale + '&include=' + includes, {
         useCache: true,
         ttl: 900 /* seconds */
      }).then(response => {
         commit('SET_STATE', { prop: 'pages', value: response.data.data })
         dispatch('PARSE_PAGES');
      });
   },

   PARSE_PAGES({ commit, getters }) {

    let parseUrl = (page) => {

       if (!page || page.url) {
          return;
       }

       let parent = page.fields.parent_page || page.fields.category;
       let parentUrl =  "/" + page.locale;
       let url;
       let attr_set = page.attribute_set_code

       // set parent for the news article
       if (attr_set == "news_article") {
          let parentPage = getters.getByProp("attribute_set_code", "blog", "pages")

          if (!parentPage || !parentPage.url) {
             parseUrl(parentPage);
          }
          parentUrl = parentPage.url
       }


       // set parent for the other entities
       if (parent) {
          let parentPage = getters.getByProp('id', parent.id, 'pages')

          if (!parentPage || !parentPage.url) {
             parseUrl(parentPage);
          }

          parentUrl = parentPage.url
       }


       page.name = page.fields.title ? page.fields.title.sanitize() : null

       if (attr_set == 'home') {
         page.url = '/';
         console.log('ja sam ovdje?', page.url)

       } else if(attr_set != "contact_info" && attr_set != "working_hours"){
          url = parentUrl + "/" + page.name
          page.url = url;
        }

    }

    let pages = getters.getPages
    _.each(pages, parseUrl)

    let orderedPages = _.orderBy(pages, function(item) {
       return parseInt(item.fields.order)
    })

    commit('SET_STATE', { prop: 'parsedPages', value: orderedPages });
 },

  PARSE_CATEGORIES({ commit, getters }) {
      let categories = getters.getManyByProp('entity_type', 'category', 'parsedPages')
      let shops = getters.getManyByProp('entity_type', 'shop', 'parsedPages')

      categories = _.orderBy(categories, function(item) {
        return item.fields.order || 10000
      })

      shops = _.orderBy(shops, function(item) {
        return item.fields.order || 10000
      })

      let cats = {};
      _.each(categories, (cat) => {
        let filteredShops = _.filter(shops, (shop) => {
            let c = shop.fields.category;
            return c.id === cat.id
        })

        cats[cat.name] = filteredShops;
      });

      commit('SET_STATE', { prop: 'parsedCategories', value: cats })
  }
};


const getters = {

   getParsedPages: state => {
      return state.parsedPages
   },

   getPages: state => {
      return state.pages
   },

   getNews: state => {
      return state.news
   },

   getParsedNews: state => {
      return state.parsedNews
   },

   getShopsByCategory: state => {

      return (cat) => {
         return state.parsedCategories[cat]
      }
   },

   getPagesForNav: state => {
      var pages = state.parsedPages.filter(page => {
         return page.fields && page.fields.in_nav
      });

      var orderedPages = _.orderBy(pages, function(item) {
         return item.fields.order
      })

      return orderedPages;
   },

   getPagesForFooter: state => {
      var pages = state.parsedPages.filter(page => {
         return page.fields && page.fields.in_footer
      });

      var orderedPages = _.orderBy(pages, function(item) {
         return item.fields.order
      })

      return orderedPages;
   },

   search: state => {
      return keyword => state.parsedPages.find(page => {
         return page.name === keyword
      });
   },

   getByUrl: state => {
      return (url) => state.parsedPages.find(page => {
         return page.url == url
      });
   },

   getManyByProp: state => {
      return (type, value) => state.parsedPages.filter(page => {
         return page[type] === value
      });
   },

   getByProp: state => {
      return (type, value, collection = "parsedPages") => state[collection].find(page => {
         // console.log('tajpaca', type, page[type], value, page.attribute_set_code);
         return page[type] == value
      });
   },

   getManyByAttr: state => {
      return (type, value) => state.parsedPages.filter(page => {
         var attr = page.fields[type];
         if (!attr) {
            return
         }
         return attr.value === value
      });
   },

   getByAttr: state => {
      return (type, value, collection = "parsedPages") => state[collection].find(page => {

         var attr = page.fields[type];

         if (!attr) {
            return
         }
         return attr === value
      });
   },

   getManyByParent: state => {
      return (value) => state.parsedPages.filter(page => {
         var attr = page.fields.parent_page || page.fields.category;

         var parent = value.fields.parent_page || value.fields.category;


         if (!attr || !value) {
            return
         }

         return attr.id === parent.id
      });
   },

   getSiblings: (state, getters) => (model) => {
      // let siblings = _.filter(getters.getManyByProp('attribute_set_code', model.attribute_set_code), function(child) {
      //    return child.id != model.id;
      // });

      let siblings = _.filter(getters.getManyByParent(model), function(child) {
         return child.id != model.id;
      });

      return siblings;
   },

   getChildren: state => {
      return (model) => state.parsedPages.filter(page => {
         var attr = page.fields.parent_page || page.fields.category;


         if (!attr) {
            return
         }


         return attr.id === model.id
      });
   }
};

const state = {
  pages: [],
  parsedPages: [],
  parsedCategories: []
};

export default {
   namespaced: true,
   actions,
   getters,
   state,
   mutations
};
